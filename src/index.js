import React from 'react';
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, NavLink, Switch } from 'react-router-dom';
//  HashRouter = is like angular with a Hash(#) at the link . change page with no reload link change
//  BrowserRouter = Common routing. change page, reload, link change.
//  MemoryRouter = when change page . no reload, link still on the root domain and everything is stored in memory

//Component
import Home from './components/home';
import Posts from './components/posts';
import Profile from './components/profiles';
import PostItem from './components/post_item';
import Life from './components/life';
import Conditional from './components/conditional';
import User from './components/user';

const App = () => {
    return (
        <BrowserRouter>
            <div>
                <header> 
                    <NavLink to="/"> Home </NavLink> <br/>
                    <NavLink 
                            to="/posts"
                            activeStyle={{color:'red'}}
                            activeClassName="selected"
                    > Posts </NavLink> <br/>
                    <NavLink to={{
                        pathname: '/profile'
                    }}> Profile </NavLink> <br/>
                    <NavLink to="/life">Life</NavLink> <br/>
                    <NavLink to="/conditional">Conditional</NavLink><br />
                    <NavLink to="/user">User</NavLink>
                    <hr />
                </header>
                {/* Opt 'exec' meant to be "ONLY THIS" */}
                <Switch>
                    {/* <Redirect from="/profile" to="/" /> */}
                    <Route path="/posts/:id/:username" component={PostItem} />
                    <Route path="/conditional" component={Conditional} />
                    <Route path="/profile" component={Profile} />
                    <Route path="/posts" component={Posts} />
                    <Route path="/life" component={Life} />
                    <Route path="/user" component={User} />
                    <Route path="/" exact component={Home} />

                    {/* this meant if the url not on the list will be render the render or you can make as component */}
                    <Route render={ () => <h3> OPPPPPS 404 </h3> } />
                </Switch>
            </div>
        </BrowserRouter>
    )
}

ReactDOM.render(<App/>, document.querySelector('#root'));