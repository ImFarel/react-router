import React, { PureComponent } from "react";

class Life extends PureComponent{
    // 1 get default props
    // 2  set default state
    state = {
        title:'LifeCycles'
    }

    // // 3 before render 
    // componentWillMount(){
    //     console.log('before render');
        
    // }

    // componentWillUpdate(){
    //     console.log('BEFORE UPDATE');
        
    // }

    // componentDidUpdate(){
    //     console.log('AFTER UPDATE');

    // }

    // shouldComponentUpdate(nextProps, nextState){
    //     if(nextState.title === this.state.title){
    //         return false;
    //     }
    //     return true;       
    // }

    // componentWillReceiveProps(){
    //     console.log('BEFORE RECEIVE PROPS');
    // }

    // componentWillUnmount(){
    //     console.log('UNMOUNT');
        
    // }
    // 4 REnder
    render(){
        console.log('render');
        
        return(
            <div>
                <h3>{this.state.title}</h3>
                <div onClick={
                    () => this.setState({
                        title: 'somethingelse'
                    })
                }> CLICK TO CHNGE

                </div>
            </div>
        )
    }

    // 5 After render
    // componentDidMount(){
    //     document.querySelector('h3').style.color = 'red';
    //     console.log('5 after render');
    // }

}
export default Life ;