import React from 'react';

const Conditional = () => {
    const val = false;

    const returnVal = () => {
        return true;
    }
    return (
        <div>
            {
                returnVal() === false ? 
                <div>
                    False
                </div>
                :
                <div>
                    True
                </div>
            }
        </div>
    )
}
export default Conditional;