import React from 'react';
import { Link } from 'react-router-dom';

// const ids = [
//     {id:'1',name:'Post 1'},
//     {id:'2',name:'Post 2'},
//     {id:'3',name:'Post 3'}
// ]

const Posts = () => {
    // return ids.map(item => {
    //     return ( 
    //         <span key={item.id}>
    //             <Link to={item.id}>{item.name}</Link> <br/>
    //         </span> 
    //     )
    // })

    return [
        <div key="0">Hello </div>, 
        <div key="1">I am </div>, 
        <div key="2">A React App.</div>
    ]
}
export default Posts;